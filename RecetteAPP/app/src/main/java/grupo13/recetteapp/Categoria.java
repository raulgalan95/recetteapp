package grupo13.recetteapp;

/**
 * Created by grupo13 on 9/6/2018.
 */

public class Categoria {

    private int idCategoria;
    private String nomCategoria;

    public Categoria() {
    }

    public Categoria(String nomCategoria) {
        this.nomCategoria = nomCategoria;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNomCategoria() {
        return nomCategoria;
    }

    public void setNomCategoria(String nomCategoria) {
        this.nomCategoria = nomCategoria;
    }
}
