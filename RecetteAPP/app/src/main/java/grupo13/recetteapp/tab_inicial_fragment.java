package grupo13.recetteapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.login.Login;
import com.facebook.login.LoginManager;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;


@SuppressLint("ValidFragment")
public class tab_inicial_fragment extends Fragment {
    EditText buscar;
    Button buscando;
    ImageButton btn;
    String nomReceta;

    private static final int RECOGNIZE_SPEECH_ACTIVITY = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmentinicial, container, false);



        btn = (ImageButton) view.findViewById(R.id.microfono);
       //buscar = (EditText) view.findViewById(R.id.buscador); //falta que busque las recetas
       // buscando=(Button) view.findViewById(R.id.consultarReceta);
        btn.setOnClickListener(onClick);

        return view;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            if(resultCode == RESULT_OK && data != null){
                ArrayList<String> speech = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                String comando = speech.get(0);

                if(comando.contains("Mostrar recetas")){

                    Intent intent = new Intent(getActivity(),BusquedaActivity.class);
                    startActivity(intent);
                }

                    //buscar.setText(strSpeech2Text);

                    // Envio de toda la información de nuestra lista al adaptador.



                    // Asignación la información anterior en el RecyclerView de nuestro layout.
               else
                    Toast.makeText(getActivity(), "Comando\""+ comando +"\" no soportado", Toast.LENGTH_SHORT).show();


                }

    }


    View.OnClickListener onClick = new View.OnClickListener() {
        public void onClick(View view) {
            Intent intentActionRecognizeSpeech = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

// Configura el Lenguaje (Español-México)
            intentActionRecognizeSpeech.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "es-MX");
            try {
                startActivityForResult(intentActionRecognizeSpeech, RECOGNIZE_SPEECH_ACTIVITY);
            } catch (ActivityNotFoundException a) {
               Toast.makeText(getActivity(),
                        "Tú dispositivo no soporta el reconocimiento por voz",
                        Toast.LENGTH_SHORT).show();
            }

        }
    };

    }

