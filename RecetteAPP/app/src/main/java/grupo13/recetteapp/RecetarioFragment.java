package grupo13.recetteapp;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class RecetarioFragment extends Fragment {
    // Instancia de RecyclerView.
    private RecyclerView recyclerViewReceta;

    // Instancia de nuestro adaptador.
    private RecyclerViewAdaptador adaptadorReceta;

    ControlBD helper;

    public RecetarioFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recetario, container, false);

        helper = new ControlBD(getContext());

        // Vinculación del RecyclerView con el de nuestro layout.
        recyclerViewReceta = (RecyclerView) view.findViewById(R.id.recyclerRecetario);

        // Forma de la lista, en este caso vertical.
        recyclerViewReceta.setLayoutManager(new LinearLayoutManager(getContext()));

        // Envio de toda la información de nuestra lista al adaptador.
        adaptadorReceta = new RecyclerViewAdaptador(obtenerRecetas());

        // Asignación la información anterior en el RecyclerView de nuestro layout.
        recyclerViewReceta.setAdapter(adaptadorReceta);

        // Inflate the layout for this fragment
        return view;
    }

    // Contiene los datos con los cuales trabajaremos.
    public List<Receta> obtenerRecetas() {
        List<Receta> recetas = new ArrayList<>();

        helper.abrir();

        Receta receta = null;

        Cursor cursor = helper.getDb().rawQuery("SELECT * FROM receta", null);

        while (cursor.moveToNext()) {
            receta = new Receta();
            receta.setIdReceta(cursor.getInt(0));
            receta.setNomReceta(cursor.getString(2));
            receta.setDescripcion(cursor.getString(3));
            receta.setImagen(cursor.getString(8));

            recetas.add(receta);
        }

        helper.cerrar();

        return recetas;
    }
}
