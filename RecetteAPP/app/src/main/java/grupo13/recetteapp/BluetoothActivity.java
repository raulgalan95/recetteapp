package grupo13.recetteapp;

import android.bluetooth.BluetoothAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

public class BluetoothActivity extends AppCompatActivity {

    Switch switchBT;

    BluetoothAdapter bluetoothAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        switchBT = (Switch) findViewById(R.id.switchBT);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter == null) {
            switchBT.setVisibility(View.GONE);
            Toast.makeText(this, R.string.no_soporta_bt, Toast.LENGTH_SHORT).show();
        } else {
            if (bluetoothAdapter.isEnabled()) {
                switchBT.setChecked(true);
            } else {
                switchBT.setChecked(false);
            }
        }
    }

    public void activarBT(View view) {
        if (bluetoothAdapter.isEnabled()) {
            if (!switchBT.isChecked()) {
                bluetoothAdapter.disable();
            }
        } else {
            if (switchBT.isChecked()) {
                bluetoothAdapter.enable();
            }
        }
    }
}
