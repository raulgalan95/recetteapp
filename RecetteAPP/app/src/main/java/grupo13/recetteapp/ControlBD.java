package grupo13.recetteapp;

/**
 * Created by grupo13 on 9/6/2018.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class ControlBD {

    // Campos de la categoria.
    private static final String[] camposCategoria = new String[]{
            "idCategoria",
            "nomCategoria"
    };

    // Campos de la receta.
    private static final String[] camposReceta = new String[]{
            "idReceta",
            "idCategoria",
            "nomReceta",
            "descripcion",
            "numPlatos",
            "elaboracion",
            "ingredientes",
            "tiempo",
            "imagen",
            "video"
    };

    // Sentencias para borrar tablas de nuestra BD.
    private static final String DROP_TABLE1 = "DROP TABLE IF EXISTS categoria;";
    private static final String DROP_TABLE2 = "DROP TABLE IF EXISTS receta;";

    // Almacena el context de nuestra Activity.
    private final Context context;

    // Auxiliar de BD.
    private DatabaseHelper DBHelper;

    // Instancia de nuestra BD.
    private SQLiteDatabase db;

    public SQLiteDatabase getDb() {
        return db;
    }

    // Constructor.
    public ControlBD(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    // Permite definir nuestra lógica para crear la BD y llevar control de las versiones de la misma.
    private static class DatabaseHelper extends SQLiteOpenHelper {

        // Nombre de nuestra BD.
        private static final String BASE_DATOS = "recetteapp.s3db";

        // Número de versión de nuestra BD.
        private static final int VERSION = 1;

        public DatabaseHelper(Context context) {
            super(context, BASE_DATOS, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                // Creando tabla categoria.
                db.execSQL("CREATE TABLE categoria(" +
                        "idCategoria INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                        "nomCategoria VARCHAR(60) NOT NULL);"
                );

                // Creando tabla receta.
                db.execSQL("CREATE TABLE receta(" +
                        "idReceta INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                        "idCategoria INTEGER NOT NULL," +
                        "nomReceta VARCHAR(60) NOT NULL," +
                        "descripcion TEXT," +
                        "numPlatos INTEGER," +
                        "elaboracion TEXT," +
                        "ingredientes TEXT," +
                        "tiempo INTEGER," +
                        "imagen VARCHAR(60)," +
                        "video VARCHAR(60));"
                );
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL(DROP_TABLE1);
                db.execSQL(DROP_TABLE2);
                onCreate(db);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // Abrir BD.
    public void abrir() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return;
    }

    // Cerrar BD.
    public void cerrar() {
        DBHelper.close();
    }

    /******************************** Métodos de la tabla categoria *******************************/

    public String insertar(Categoria categoria) {
        String regInsertados = context.getString(R.string.catInsertadas);
        long contador = 0;

        ContentValues cat = new ContentValues();
        cat.put("nomCategoria", categoria.getNomCategoria());

        contador = db.insert("categoria", null, cat);

        regInsertados = regInsertados + " " + contador;

        return regInsertados;
    }

    public Categoria consultarCategoria(int idCategoria) {
        String[] id = {String.valueOf(idCategoria)};

        Cursor cursor = db.query("categoria", camposCategoria, "idCategoria = ?", id, null, null, null);

        if (cursor.moveToFirst()) {
            Categoria categoria = new Categoria();
            categoria.setIdCategoria(cursor.getInt(0));
            categoria.setNomCategoria(cursor.getString(1));
            return categoria;
        } else {
            return null;
        }
    }

    public String actualizar(Categoria categoria) {
        String[] id = {String.valueOf(categoria.getIdCategoria())};

        ContentValues cv = new ContentValues();
        cv.put("nomCategoria", categoria.getNomCategoria());

        db.update("categoria", cv, "idCategoria = ?", id);

        return context.getString(R.string.catActualizada);
    }

    public String eliminar(Categoria categoria) {

        // Verificar si existe la categoria.
        if (verificarIntegridad(categoria, 4)) {

            // Verificar si tiene registros relacionados.
            if (verificarIntegridad(categoria, 1)) {
                return context.getString(R.string.catNoEliminada);
            } else {
                db.delete("categoria", "idCategoria = " + categoria.getIdCategoria(), null);
                return context.getString(R.string.catEliminada);
            }
        } else {
            return context.getString(R.string.catNoExiste);
        }

    }

    /********************************* Métodos de la tabla receta *********************************/

    public String insertar(Receta receta) {
        String regInsertados = context.getString(R.string.recInsertadas);
        long contador = 0;

        // Verificar integridad referencial.
        if (verificarIntegridad(receta, 2)) {
            ContentValues rec = new ContentValues();
            rec.put("idCategoria", receta.getIdCategoria());
            rec.put("nomReceta", receta.getNomReceta());
            rec.put("descripcion", receta.getDescripcion());
            rec.put("numPlatos", receta.getNumPlatos());
            rec.put("elaboracion", receta.getElaboracion());
            rec.put("ingredientes", receta.getIngredientes());
            rec.put("tiempo", receta.getTiempo());
            rec.put("imagen", receta.getImagen());
            rec.put("video", receta.getVideo());

            contador = db.insert("receta", null, rec);

            regInsertados = regInsertados + " " + contador;
        } else {
            regInsertados = context.getString(R.string.recCategoria);
        }

        return regInsertados;
    }

    public long insertar2(Receta receta) {
        long contador = 0;

        // Verificar integridad referencial.
        if (verificarIntegridad(receta, 2)) {
            ContentValues rec = new ContentValues();
            rec.put("idCategoria", receta.getIdCategoria());
            rec.put("nomReceta", receta.getNomReceta());
            rec.put("descripcion", receta.getDescripcion());
            rec.put("numPlatos", receta.getNumPlatos());
            rec.put("elaboracion", receta.getElaboracion());
            rec.put("ingredientes", receta.getIngredientes());
            rec.put("tiempo", receta.getTiempo());
            rec.put("imagen", receta.getImagen());
            rec.put("video", receta.getVideo());

            contador = db.insert("receta", null, rec);
        }

        return contador;
    }

    public Receta consultarReceta(int idReceta) {
        String[] id = {String.valueOf(idReceta)};

        Cursor cursor = db.query("receta", camposReceta, "idReceta = ?", id, null, null, null);

        if (cursor.moveToFirst()) {
            Receta receta = new Receta();
            receta.setIdReceta(cursor.getInt(0));
            receta.setIdCategoria(cursor.getInt(1));
            receta.setNomReceta(cursor.getString(2));
            receta.setDescripcion(cursor.getString(3));
            receta.setNumPlatos(cursor.getInt(4));
            receta.setElaboracion(cursor.getString(5));
            receta.setIngredientes(cursor.getString(6));
            receta.setTiempo(cursor.getInt(7));
            receta.setImagen(cursor.getString(8));
            receta.setVideo(cursor.getString(9));
            return receta;
        } else {
            return null;
        }
    }

    public String actualizar(Receta receta) {
        String[] id = {String.valueOf(receta.getIdReceta())};

        // Verificar integridad referencial.
        if (verificarIntegridad(receta, 2)) {
            ContentValues cv = new ContentValues();
            cv.put("idCategoria", receta.getIdCategoria());
            //cv.put("nomReceta", receta.getNomReceta());
           // cv.put("descripcion", receta.getDescripcion());
            cv.put("numPlatos", receta.getNumPlatos());
            cv.put("elaboracion", receta.getElaboracion());
            cv.put("ingredientes", receta.getIngredientes());
            cv.put("tiempo", receta.getTiempo());
            //cv.put("imagen", receta.getImagen());
            //cv.put("video", receta.getVideo());

            db.update("receta", cv, "idReceta = ?", id);

            return context.getString(R.string.recActualizada);
        } else {
            return context.getString(R.string.recCategoria);
        }
    }

    public String eliminar(Receta receta) {

        // Verificar si existe la receta.
        if (verificarIntegridad(receta, 3)) {
            db.delete("receta", "idReceta = " + receta.getIdReceta(), null);
            return context.getString(R.string.recEliminada);
        } else {
            return context.getString(R.string.recNoExiste);
        }
    }

    /************************************ Verificar integridad ************************************/

    private boolean verificarIntegridad(Object dato, int relacion) throws SQLException {

        switch (relacion) {

            // Verificar si la categoria tiene recetas relacionadas.
            case 1: {
                Categoria categoria = (Categoria) dato;
                abrir();
                Cursor cursor = db.query(true, "receta", new String[]{"idCategoria"}, "idCategoria = " + categoria.getIdCategoria(), null, null, null, null, null);
                if (cursor.moveToFirst())
                    return true; // Si tiene recetas relacionadas.
                else
                    return false; // No tiene recetas relacionadas.
            }

            // Verifica si existe la categoria a la que hace referencia la receta.
            case 2: {
                Receta receta = (Receta) dato;
                String[] id = {String.valueOf(receta.getIdCategoria())};
                abrir();
                Cursor cursor = db.query("categoria", null, "idCategoria = ?", id, null, null, null);
                if (cursor.moveToFirst()) {
                    return true; // Si existe la categoria.
                }
                return false; // No existe la categoria.
            }

            // Verifica si existe una receta con el id proporcionado.
            case 3: {
                Receta receta = (Receta) dato;
                String[] id = {String.valueOf(receta.getIdReceta())};
                abrir();
                Cursor cursor = db.query("receta", null, "idReceta = ?", id, null, null, null);
                if (cursor.moveToFirst()) {
                    return true; // Si existe una receta con el id proporcionado.
                }
                return false; // No existe una receta con el id proporcionado.
            }

            // Verifica si existe una categoria con el id proporcionado.
            case 4: {
                Categoria categoria = (Categoria) dato;
                String[] id = {String.valueOf(categoria.getIdCategoria())};
                abrir();
                Cursor cursor = db.query("categoria", null, "idCategoria = ?", id, null, null, null);
                if (cursor.moveToFirst()) {
                    return true; // Si existe una categoria con el id proporcionado.
                }
                return false; // No existe una categoria con el id proporcionado.
            }

            default:
                return false;
        }
    }

    /********************************** Llenado de base de datos **********************************/

    public String llenarBD() {
        abrir();

        db.execSQL("DELETE FROM categoria");
        db.execSQL("DELETE FROM receta");

        // Llenado de la tabla categoría.
        final String[] vNomCategoria = {"Ensalada", "Sopa"};

        Categoria categoria = new Categoria();
        for (int i = 0; i < 2; i++) {
            categoria.setNomCategoria(vNomCategoria[i]);
            insertar(categoria);
        }

        // Llenado de la tabla receta.
        String elaboracion1 = "1. Hervir los calamares en abundante agua con una pastilla de Avecrem caldo de Pescado -30% de sal durante 15 minutos. Escurrir y enfriar.\n" +
                "2. Cortar los pimientos en tiras y colocar en la ensaladera a capas: pimientos, calamares, cebolleta en aros, atún desmenuzado, alcaparras y huevos duros picados.\n" +
                "3. Preparar una vinagreta mezclando el aceite, el vinagre, una pizca de Avecrem Ajo y Perejil Granulado hasta que quede bien ligado, y cubrir la ensalada.";

        String elaboracion2 = "1. Calienta en una olla mediana a fuego medio el aceite. Fríe el pollo por 5 minutos moviendo constantemente para evitar que se pegue.\n" +
                "2. Agrega el poro, el pimiento, las espinacas, salpimienta y cuece por 5 minutos más.\n" +
                "3. Retira de la olla la mezcla anterior y reserva.\n" +
                "4. Agrega el caldo de pollo Campbells® a la olla y cuando rompa el hervor incorpora el fideo. Cuece por 5 minutos.\n" +
                "5. Agrega la mezcla de pimiento y calienta por 5 minutos más.";

        String ingredientes1 = "1/2 kg. de aros de calamar\n" +
                "1 pellizco de Avecrem Ajo y Perejil Granulado\n" +
                "Vinagre\n" +
                "1 lata de atún\n" +
                "1 bote de pimientos asados\n" +
                "1 pastilla de Avecrem Caldo de Pescado\n" +
                "1 dl de aceite\n" +
                "1 cucharada de alcaparras\n" +
                "3 cebolletas\n" +
                "4 huevos duros";

        String ingredientes2 = "2 cucharadas de aceite de ajonjolí\n" +
                "250 gramos de pechuga de pollo sin hueso, sin piel y cortada en cubos medianos\n" +
                "2 cucharadas de poro cortado en rodajas finas\n" +
                "1 pieza de pimiento morrón rojo cortado en tiras delgadas\n" +
                "1 taza de espinaca baby\n" +
                "2 pizcas de sal\n" +
                "1 pizca de pimienta negra molida\n" +
                "1 litro de caldo de pollo Campbells\n" +
                "1/2 paquete de fideo de arroz chino (170 g)\n";

        final String[] vNomReceta = {"Ensalada meditérranea", "Sopa de fideos chinos"};
        final String[] vDescripcion = {"Ensalada con calamar", "Deliciosa sopa de fideos fácil de cocinar"};
        final int[] vNumPlatos = {4, 4};
        final String[] vElaboracion = {elaboracion1, elaboracion2};
        final String[] vIngredientes = {ingredientes1, ingredientes2};
        final int[] vTiempo = {20, 30};
        final String[] vVideo = {"byZIwcDrwwI","dIC9iD5KaYI"};

        List<Categoria> categorias = new ArrayList<>();

        Categoria cat = null;

        Cursor cursor = db.rawQuery("SELECT * FROM categoria", null);

        while (cursor.moveToNext()) {
            cat = new Categoria();
            cat.setIdCategoria(cursor.getInt(0));
            cat.setNomCategoria(cursor.getString(1));

            categorias.add(cat);
        }

        final int[] vIdCategoria = {categorias.get(0).getIdCategoria(), categorias.get(1).getIdCategoria()};

        Receta receta = new Receta();
        for (int i = 0; i < 2; i++) {
            receta.setIdCategoria(vIdCategoria[i]);
            receta.setNomReceta(vNomReceta[i]);
            receta.setDescripcion(vDescripcion[i]);
            receta.setNumPlatos(vNumPlatos[i]);
            receta.setElaboracion(vElaboracion[i]);
            receta.setIngredientes(vIngredientes[i]);
            receta.setTiempo(vTiempo[i]);
            receta.setVideo(vVideo[i]);
            insertar(receta);
        }

        cerrar();

        return context.getString(R.string.guardado);
    }
}
