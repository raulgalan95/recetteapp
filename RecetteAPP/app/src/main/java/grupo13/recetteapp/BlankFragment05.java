package grupo13.recetteapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.File;


public class BlankFragment05 extends Fragment {

    Button Play;
    Button Stop;
    VideoView video;
    MediaController mediacontrol;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank_fragment05, container, false);
        video = (VideoView) view.findViewById(R.id.video);
        String path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.valores;
        video.setVideoURI(Uri.parse(path));
        video.start();

        return view;
    }

}