package grupo13.recetteapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

@SuppressLint("ValidFragment")
public class MRIngredientesFragment extends Fragment {

    private String ingredientes;
    private String imagen;

    TextView txtIngredientes;

    TextToSpeech tts;
    Button ttsIngredientes;

    public MRIngredientesFragment(String ingredientes, String imagen) {
        this.ingredientes = ingredientes;
        this.imagen = imagen;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mringredientes, container, false);

        txtIngredientes = (TextView) view.findViewById(R.id.txtIngredientes);

        txtIngredientes.setText(ingredientes);

        ttsIngredientes = (Button) view.findViewById(R.id.ttsIngredientes);
        tts = new TextToSpeech(getContext(), OnInit);
        ttsIngredientes.setOnClickListener(onClick);

        return view;
    }

    TextToSpeech.OnInitListener OnInit = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            // TODO Auto-generated method stub
            if (TextToSpeech.SUCCESS == status) {
                tts.setLanguage(new Locale("spa", "ESP"));
            } else {
                Toast.makeText(getContext(), "TTS no disponible", Toast.LENGTH_SHORT).show();
            }
        }
    };

    View.OnClickListener onClick = new View.OnClickListener() {
        @SuppressLint("SdCardPath")
        public void onClick(View v) {
            // TODO Auto-generated method stub
            switch (v.getId()) {
                case R.id.ttsIngredientes:
                    tts.speak("Ingredientes de la receta: " + txtIngredientes.getText().toString(), TextToSpeech.QUEUE_ADD, null);
                    break;
            }
        }
    };

    public void onDestroy() {
        tts.shutdown();
        super.onDestroy();
    }
}
