package grupo13.recetteapp;

/**
 * Created by grupo13 on 9/6/2018.
 */

public class Receta {

    private int idReceta;
    private int idCategoria;
    private String nomReceta;
    private String descripcion;
    private int numPlatos;
    private String elaboracion;
    private String ingredientes;
    private int tiempo;
    private String imagen;
    private String video;

    public Receta() {
    }

    public Receta(int idCategoria, String nomReceta, String descripcion, int numPlatos, String elaboracion, String ingredientes, int tiempo, String imagen, String video) {
        this.idCategoria = idCategoria;
        this.nomReceta = nomReceta;
        this.descripcion = descripcion;
        this.numPlatos = numPlatos;
        this.elaboracion = elaboracion;
        this.ingredientes = ingredientes;
        this.tiempo = tiempo;
        this.imagen = imagen;
        this.video = video;
    }

    public int getIdReceta() {
        return idReceta;
    }

    public void setIdReceta(int idReceta) {
        this.idReceta = idReceta;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNomReceta() {
        return nomReceta;
    }

    public void setNomReceta(String nomReceta) {
        this.nomReceta = nomReceta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getNumPlatos() {
        return numPlatos;
    }

    public void setNumPlatos(int numPlatos) {
        this.numPlatos = numPlatos;
    }

    public String getElaboracion() {
        return elaboracion;
    }

    public void setElaboracion(String elaboracion) {
        this.elaboracion = elaboracion;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
}
