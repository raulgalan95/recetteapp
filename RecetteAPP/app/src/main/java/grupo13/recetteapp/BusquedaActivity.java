package grupo13.recetteapp;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class BusquedaActivity extends AppCompatActivity {

    // Instancia de RecyclerView.
    private RecyclerView recyclerViewReceta;

    // Instancia de nuestro adaptador.
    private RecyclerViewAdaptador adaptadorReceta;

    ControlBD helper;

    public BusquedaActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);
        helper = new ControlBD(this);

        // Vinculación del RecyclerView con el de nuestro layout.
        recyclerViewReceta = (RecyclerView) findViewById(R.id.recyclerBusqueda);

        // Forma de la lista, en este caso vertical.
        recyclerViewReceta.setLayoutManager(new LinearLayoutManager(this));

        // Envio de toda la información de nuestra lista al adaptador.
        adaptadorReceta = new RecyclerViewAdaptador(obtenerRecetas());

        // Asignación la información anterior en el RecyclerView de nuestro layout.
        recyclerViewReceta.setAdapter(adaptadorReceta);
    }
    // Contiene los datos con los cuales trabajaremos.
    public List<Receta> obtenerRecetas() {
        List<Receta> recetas = new ArrayList<>();

        helper.abrir();

        Receta receta = null;

        Cursor cursor = helper.getDb().rawQuery("SELECT * FROM receta", null);

        while (cursor.moveToNext()) {
            receta = new Receta();
            receta.setIdReceta(cursor.getInt(0));
            receta.setNomReceta(cursor.getString(2));
            receta.setDescripcion(cursor.getString(3));
            receta.setImagen(cursor.getString(8));

            recetas.add(receta);
        }

        helper.cerrar();

        return recetas;
    }
}
