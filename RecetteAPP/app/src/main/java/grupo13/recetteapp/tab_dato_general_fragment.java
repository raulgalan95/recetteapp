package grupo13.recetteapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;


public class tab_dato_general_fragment extends Fragment {

    Button btn;
    Button btnGuardar;
    private static final int CAMARA_REQUEST = 123;
    ImageView imageView;
    Uri file;
    ControlBD helper;
    Bundle bundle;
    Receta receta;
    EditText editTitulo;
    EditText editCategoria;
    EditText editLink;
    EditText editDescripcion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_dato_general, container, false);
       // bundle = getActivity().getIntent().getExtras();



        btn = (Button) view.findViewById(R.id.idbtn);
        imageView = (ImageView) view.findViewById(R.id.imageView);
        btn.setOnClickListener(onClick);

        /*
        //Lo nuevo

        if(savedInstanceState !=null)
        {
            if(savedInstanceState.getString("Foto")!= null)
            {
                imageView.setImageURI(Uri.parse(savedInstanceState.getString("Foto")));
            }
        }*/
        return view;
    }

    View.OnClickListener onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            startActivityForResult(intent,CAMARA_REQUEST);
        }
    };


    public void onActivityResult(int requestCode,int resultCode,Intent data)
    {
        if(requestCode == CAMARA_REQUEST && resultCode == Activity.RESULT_OK)
        {
            Bitmap photo = (Bitmap) data.getExtras().get("data");

            //imageView.setImageURI(file);
            imageView.setImageBitmap(photo);
        }

    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = new ControlBD(getActivity());
        btnGuardar = (Button) view.findViewById(R.id.button2);
        editTitulo = (EditText) view.findViewById(R.id.titulorece);
        editCategoria = (EditText) view.findViewById(R.id.catego);
        editLink = (EditText) view.findViewById(R.id.Linkvideo);
        editDescripcion = (EditText) view.findViewById(R.id.descrip);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String titulo = editTitulo.getText().toString();
                String catego = editCategoria.getText().toString();
                String link = editLink.getText().toString();
                String des = editDescripcion.getText().toString();
                String regInsertados;
                Receta receta = new Receta();
                receta.setIdCategoria(Integer.parseInt(catego));
                receta.setNomReceta(titulo);
                receta.setVideo(link);
                receta.setDescripcion(des);

                helper.abrir();
                regInsertados = helper.insertar(receta);
                helper.cerrar();
                Toast.makeText(getActivity(), regInsertados, Toast.LENGTH_SHORT).show();

            }
        });
    }



}
