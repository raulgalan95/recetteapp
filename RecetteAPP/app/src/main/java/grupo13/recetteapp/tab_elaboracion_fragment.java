package grupo13.recetteapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class tab_elaboracion_fragment extends Fragment {
    Button btnGuardar;
    EditText editCat;
    EditText editIngre;
    EditText editElabo;
    EditText editnombre;
    EditText editplatos;
    EditText editTime;
    ControlBD helper;
    Bundle bundle;
    Receta receta;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab_elaboracion, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        helper=new ControlBD(getActivity());
        btnGuardar = (Button) view.findViewById(R.id.guardar);
        editIngre = (EditText) view.findViewById(R.id.ingre);
        editElabo = (EditText) view.findViewById(R.id.elabo);
        editCat = (EditText) view.findViewById(R.id.cat);
        editnombre = (EditText) view.findViewById(R.id.nombre);
        editplatos = (EditText) view.findViewById(R.id.plato);
        editTime = (EditText) view.findViewById(R.id.tiempo);

        btnGuardar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String categoria=editCat.getText().toString();
                String ingrediente=editIngre.getText().toString();
                String elaboracion=editElabo.getText().toString();
                String nombre=editnombre.getText().toString();
                String plato=editplatos.getText().toString();
                String tiempo=editTime.getText().toString();
                String regInsertados;
                Receta receta=new Receta();
                receta.setIdCategoria(Integer.parseInt(categoria));
                receta.setIdReceta(Integer.parseInt(nombre));
                receta.setIngredientes(ingrediente);
                receta.setElaboracion(elaboracion);
                receta.setNumPlatos(Integer.parseInt(plato));
                receta.setTiempo(Integer.parseInt(tiempo));
                helper.abrir();
                regInsertados=helper.actualizar(receta);
                helper.cerrar();
                Toast.makeText(getActivity(),regInsertados, Toast.LENGTH_SHORT).show();

            }
        });

    }
}
