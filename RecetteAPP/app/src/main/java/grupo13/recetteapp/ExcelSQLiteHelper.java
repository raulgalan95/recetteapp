package grupo13.recetteapp;

import android.content.ContentValues;
import android.util.Log;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.Iterator;

/**
 * Created by grupo13 on 14/6/2018.
 */

public class ExcelSQLiteHelper {

    public static int insertarExcelASqlite(ControlBD controlBD, Sheet sheet) {
        int contador = 0;
        for (Iterator<Row> rit = sheet.rowIterator(); rit.hasNext();) {
            Row row = rit.next();

            row.getCell(0, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_NUMERIC); // idCategoria
            row.getCell(1, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_STRING);  // nomReceta
            row.getCell(2, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_STRING);  // descripcion
            row.getCell(3, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_NUMERIC); // numPlatos
            row.getCell(4, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_STRING);  // elaboracion
            row.getCell(5, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_STRING);  // ingredientes
            row.getCell(6, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_NUMERIC); // tiempo
            row.getCell(7, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_STRING);  // imagen
            row.getCell(8, Row.CREATE_NULL_AS_BLANK).setCellType(Cell.CELL_TYPE_STRING);  // video

            Receta receta = new Receta();

            receta.setIdCategoria((int)row.getCell(0, Row.CREATE_NULL_AS_BLANK).getNumericCellValue());
            receta.setNomReceta(row.getCell(1, Row.CREATE_NULL_AS_BLANK).getStringCellValue());
            receta.setDescripcion(row.getCell(2, Row.CREATE_NULL_AS_BLANK).getStringCellValue());
            receta.setNumPlatos((int)row.getCell(3, Row.CREATE_NULL_AS_BLANK).getNumericCellValue());
            receta.setElaboracion(row.getCell(4, Row.CREATE_NULL_AS_BLANK).getStringCellValue());
            receta.setIngredientes(row.getCell(5, Row.CREATE_NULL_AS_BLANK).getStringCellValue());
            receta.setTiempo((int)row.getCell(6, Row.CREATE_NULL_AS_BLANK).getNumericCellValue());
            receta.setImagen(row.getCell(7, Row.CREATE_NULL_AS_BLANK).getStringCellValue());
            receta.setVideo(row.getCell(8, Row.CREATE_NULL_AS_BLANK).getStringCellValue());

            try {
                long respuesta = controlBD.insertar2(receta);
                if (respuesta > 0) {
                    contador++;
                }
            } catch (Exception e) {
                Log.d("Excepcion: ", e.getMessage().toString());
            }
        }
        return contador;
    }
}
