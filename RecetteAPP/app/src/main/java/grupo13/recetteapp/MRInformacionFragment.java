package grupo13.recetteapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Locale;

@SuppressLint("ValidFragment")
public class MRInformacionFragment extends Fragment {

    private int idCategoria;
    private String nomReceta;
    private String descripcion;
    private int numPlatos;
    private int tiempo;
    private String elaboracion;

    private long timeLeftInMilliseconds=600000;
    private boolean timerRunning;

    TextView txtNombre;
    TextView txtCategoria;
    TextView txtDescripcion;
    TextView txtNumPlatos;
    TextView txtTiempo;
    TextView txtElaboracion;
    //TextView countdownText;
    ControlBD helper;

    Categoria categoria;

    TextToSpeech tts;
    Button ttsNombre;
    Button ttsCategoria;
    Button ttsDescripcion;
    Button ttsNumPlatos;
    Button ttsTiempo;
    Button ttsElaboracion;
    //Button countdownButton;


    //CountDownTimer countDownTimer;

    public MRInformacionFragment(int idCategoria, String nomReceta, String descripcion, int numPlatos, int tiempo, String elaboracion) {
        this.idCategoria = idCategoria;
        this.nomReceta = nomReceta;
        this.descripcion = descripcion;
        this.numPlatos = numPlatos;
        this.tiempo = tiempo;
        this.elaboracion = elaboracion;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mrinformacion, container, false);

        helper = new ControlBD(getContext());

        helper.abrir();
        categoria = helper.consultarCategoria(idCategoria);
        helper.cerrar();

        txtNombre = (TextView) view.findViewById(R.id.txtNombre);
        txtCategoria = (TextView) view.findViewById(R.id.txtCategoria);
        txtDescripcion = (TextView) view.findViewById(R.id.txtDescripcion);
        txtNumPlatos = (TextView) view.findViewById(R.id.txtNumPlatos);
        txtTiempo = (TextView) view.findViewById(R.id.txtTiempo);
        txtElaboracion = (TextView) view.findViewById(R.id.txtElaboracion);
      //  countdownText = (TextView) view.findViewById(R.id.countdown);


        txtNombre.setText(nomReceta);
        txtCategoria.setText(categoria.getNomCategoria());
        txtDescripcion.setText(descripcion);
        txtNumPlatos.setText(String.valueOf(numPlatos));
        txtElaboracion.setText(elaboracion);

        if (tiempo < 60) {
            txtTiempo.setText(String.valueOf(tiempo) + " min");

        } else {
            int horas = tiempo / 60;
            if (tiempo == 60) {
                txtTiempo.setText(String.valueOf(horas) + " hora");
            } else {
                txtTiempo.setText(String.valueOf(horas) + " horas " + String.valueOf(tiempo % 60) + " min");
            }
        }
       // countdownButton = (Button) view.findViewById(R.id.countdownButton);
        ttsNombre = (Button) view.findViewById(R.id.ttsNombre);
        ttsCategoria = (Button) view.findViewById(R.id.ttsCategoria);
        ttsDescripcion = (Button) view.findViewById(R.id.ttsDescripcion);
        ttsNumPlatos = (Button) view.findViewById(R.id.ttsNumPlatos);
        ttsTiempo = (Button) view.findViewById(R.id.ttsTiempo);
        ttsElaboracion = (Button) view.findViewById(R.id.ttsElaboracion);
        tts = new TextToSpeech(getContext(), OnInit);
        ttsNombre.setOnClickListener(onClick);
        ttsCategoria.setOnClickListener(onClick);
        ttsDescripcion.setOnClickListener(onClick);
        ttsNumPlatos.setOnClickListener(onClick);
        ttsTiempo.setOnClickListener(onClick);
        ttsElaboracion.setOnClickListener(onClick);

      //  countdownButton.setOnClickListener(onClick);



        timeLeftInMilliseconds = (60000*tiempo);
        return view;
    }

    TextToSpeech.OnInitListener OnInit = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            // TODO Auto-generated method stub
            if (TextToSpeech.SUCCESS == status) {
                tts.setLanguage(new Locale("spa", "ESP"));
            } else {
                Toast.makeText(getContext(), "TTS no disponible", Toast.LENGTH_SHORT).show();
            }
        }
    };

    View.OnClickListener onClick = new View.OnClickListener() {
        @SuppressLint("SdCardPath")
        public void onClick(View v) {
            // TODO Auto-generated method stub
            switch (v.getId()) {
                case R.id.ttsNombre:
                    tts.speak("Nombre de la receta: " + txtNombre.getText().toString(), TextToSpeech.QUEUE_ADD, null);
                    break;
                case R.id.ttsCategoria:
                    tts.speak("Categoría de la receta: " + txtCategoria.getText().toString(), TextToSpeech.QUEUE_ADD, null);
                    break;
                case R.id.ttsDescripcion:
                    tts.speak("Descripción de la receta: " + txtDescripcion.getText().toString(), TextToSpeech.QUEUE_ADD, null);
                    break;
                case R.id.ttsNumPlatos:
                    tts.speak("Número de platos: " + txtNumPlatos.getText().toString(), TextToSpeech.QUEUE_ADD, null);
                    break;
                case R.id.ttsTiempo:
                    tts.speak("Tiempo de elaboración de la receta: " + txtTiempo.getText().toString(), TextToSpeech.QUEUE_ADD, null);
                    break;
                case R.id.ttsElaboracion:
                    tts.speak("Pasos para elaborar la receta: " + txtElaboracion.getText().toString(), TextToSpeech.QUEUE_ADD, null);
                    break;
            }

        }
    };
/*
    public void startStop()
    {
        if(timerRunning)
        {
            stopTimer();
        }else
        {startTimer();}
    }

    public void startTimer()
    {
        countDownTimer= new CountDownTimer(timeLeftInMilliseconds,1000){

            @Override
            public void onTick(long l) {
                countdownText.setText(""+timeLeftInMilliseconds/1000);
                timeLeftInMilliseconds=1;
                updateTimer();
            }

            @Override
            public void onFinish() {

            }
        }.start();
        countdownButton.setText("pause");
        timerRunning=true;
    }

    public void stopTimer()
    {
        countDownTimer.cancel();
        countdownButton.setText("play");
        timerRunning=false;
    }

    public void updateTimer()
    {
        int minutes = (int) timeLeftInMilliseconds /60000;
        int seconds = (int) timeLeftInMilliseconds % 60000/1000;

        String timeLeftText;
        timeLeftText ="" + minutes;
        timeLeftText += ":";
        if(seconds<60) timeLeftText +="0";
        timeLeftText += seconds;

        countdownText.setText(timeLeftText);
    }
*/
    public void onDestroy() {
        tts.shutdown();
        super.onDestroy();
    }


}
