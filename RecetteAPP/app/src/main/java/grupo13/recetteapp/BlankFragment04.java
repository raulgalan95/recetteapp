package grupo13.recetteapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static android.app.Activity.RESULT_OK;

// Importar de Excel.
public class BlankFragment04 extends Fragment {

    ControlBD controlBD;
    Button btnImportar;
    public static final int requestcode = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank_fragment04, container, false);

        btnImportar = (Button) view.findViewById(R.id.btnSubir);

        btnImportar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
                fileIntent.setType("*/*");
                try {
                    startActivityForResult(fileIntent, requestcode);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getContext(), getString(R.string.noArchivos), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        switch (requestCode) {
            case requestcode:
                //String FilePath = "/storage/sdcard/Download/prueba.xls";

                String path = data.getData().getPath();
                String[] p = path.split(":");
                Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath());
                String FilePath = uri + "/" + p[1];

                try {
                    if (resultCode == RESULT_OK) {
                        AssetManager am = getContext().getAssets();
                        InputStream inStream;
                        Workbook wb = null;
                        try {
                            inStream = new FileInputStream(FilePath);
                            wb = new HSSFWorkbook(inStream);
                            inStream.close();
                        } catch (IOException e) {
                            Toast.makeText(getContext(), getString(R.string.noEncontrado), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }

                        controlBD = new ControlBD(getContext());

                        Sheet sheet = wb.getSheetAt(0);

                        if (sheet == null) {
                            Toast.makeText(getContext(), getString(R.string.docVacio), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        controlBD.abrir();
                        int contador = ExcelSQLiteHelper.insertarExcelASqlite(controlBD, sheet);
                        controlBD.cerrar();

                        Toast.makeText(getContext(), getString(R.string.recetasRegistradas) + " " + contador, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }
}
