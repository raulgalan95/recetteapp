package grupo13.recetteapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;

/**
 * Created by grupo13 on 10/6/2018.
 * Comunica al RecyclerView de nuestro layout la cantidad de información que se utilizará para
 * crear y poblar cada item de nuestra lista. Puente entre presentación e información a mostrar.
 */

public class RecyclerViewAdaptador extends RecyclerView.Adapter<RecyclerViewAdaptador.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Context context;
        int idReceta;

        TextView txtReceta;
        TextView txtDescripcion;
        ImageView imgReceta;
        Button btnVer;

        public ViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();

            txtReceta = (TextView) itemView.findViewById(R.id.txtReceta);
            txtDescripcion = (TextView) itemView.findViewById(R.id.txtDescripcion);
            imgReceta = (ImageView) itemView.findViewById(R.id.imgReceta);
            btnVer = (Button) itemView.findViewById(R.id.btnVer);
        }

        void setOnClickListeners(int id) {
            btnVer.setOnClickListener(this);
            idReceta = id;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnVer:
                    Intent intent = new Intent(context, MostrarRecetaActivity.class);
                    intent.putExtra("idReceta", idReceta);
                    context.startActivity(intent);
                    break;
            }
        }
    }

    // Lista para almacenar todos los datos mostrados en cada item.
    public List<Receta> recetaLista;

    // Constructor del adaptador.
    public RecyclerViewAdaptador(List<Receta> recetaLista) {
        this.recetaLista = recetaLista;
    }

    // Infla el contenido de un nuevo item para la lista.
    // Inflar es el procedimiento que se realiza para hacer uso de un layout dentro de otro layout.
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recetario, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // Realiza las modificaciones del contenido para cada item.
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtReceta.setText(recetaLista.get(position).getNomReceta());
        holder.txtDescripcion.setText(recetaLista.get(position).getDescripcion());
        if (recetaLista.get(position).getImagen() == null || recetaLista.get(position).getImagen() == "") {
            holder.imgReceta.setImageResource(R.drawable.sinfoto);
        } else {
            //holder.imgReceta.setImageURI(Uri.parse(savedInstanceState.getString(recetaLista.get(position).getImagen())));
            holder.imgReceta.setImageResource(R.drawable.sinfoto);
        }

        holder.setOnClickListeners(recetaLista.get(position).getIdReceta());
    }

    // Indica al adaptador la cantidad de elementos que se procesaran.
    @Override
    public int getItemCount() {
        return recetaLista.size();
    }
}
